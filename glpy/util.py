# -*- coding: utf-8 -*-

"""
glpy.util
~~~~~~~~~

Utility functions for this test project.
"""


def add_surrounding_space(string):
    """Returns a string surrounded with space."""
    return f" {string} "
