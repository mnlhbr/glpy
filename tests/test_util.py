# -*- coding: utf-8 -*-


import unittest
import glpy.util


class TestSurroundingSpace(unittest.TestCase):

    def test_add_surrounding_space(self):
        self.assertEqual(" 42 ", glpy.util.add_surrounding_space("42"))
